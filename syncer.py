from bitrix import Bitrix
from yougile import YouGile
from db.models import Task, Column, Comment, Project

class Syncer:
    def __init__(self) -> None:
        self.bitrix = Bitrix()
        self.yougile = YouGile()
        self.session = None # Сессия нашей БД

    def create_task_from_bitrix(self, bitrix_task_id: int):
        bitrix_task = self.bitrix.get_task(bitrix_task_id)

        task = Task() # Заводим таску в БД

        yougile_task_id = self.yougile.create_task(task.convert_to_yougile_params())

        task.set_yougile_id(yougile_task_id)

    def create_task_from_yougile(self, yougile_task_id: int):
        yougile_task = self.yougile.get_task(yougile_task_id)

        task = Task() # Заводим задачу в БД

        bitrix_task_id = self.bitrix.create_task(task.convert_to_bitrix_params())

        task.set_bitrix_id(bitrix_task_id)
from flask import Flask, request, Response
from http import HTTPStatus
from flask.logging import default_handler
import logging
from urllib.parse import parse_qs, urlparse
from syncer import Syncer


app = Flask(__name__)

syncer = Syncer()

def resolve_bitrix_webhook(event: str, entity_id: int):

    if event == "ONTASKADD":
        syncer.create_task_from_bitrix(entity_id)

@app.route("/api/bitrix_handler", methods=['POST'])
def handle_bitrix_webhook():
    app.logger.info(request.path)
    app.logger.info(request.method)
    app.logger.info(request.headers)
    bitrix_data = request.get_data().decode('utf-8')
    # event=ONTASKUPDATE&data%5BFIELDS_BEFORE%5D%5BID%5D=40003&data%5BFIELDS_AFTER%5D%5BID%5D=40003&data%5BIS_ACCESSIBLE_BEFORE%5D=undefined&data%5BIS_ACCESSIBLE_AFTER%5D=undefined&ts=1671351541&auth%5Bdomain%5D=truecode.bitrix24.ru&auth%5Bclient_endpoint%5D=https%3A%2F%2Ftruecode.bitrix24.ru%2Frest%2F&auth%5Bserver_endpoint%5D=https%3A%2F%2Foauth.bitrix.info%2Frest%2F&auth%5Bmember_id%5D=7ead9a280610d866dbc7c535d1521295&auth%5Bapplication_token%5D=yp71ffgv4w0zoo7ygfisplc7ar5i1jl1
    bitrix_data = parse_qs(bitrix_data)
    app.logger.info(bitrix_data)

    event = bitrix_data['event'][0]
    entity_id = int(bitrix_data['data[FIELDS_AFTER][ID]'][0])

    resolve_bitrix_webhook(event, entity_id)

    return Response(status=HTTPStatus.OK)

@app.route("/api/yougile_handler", methods=['POST'])
def handle_yougile_webhook():
    app.logger.info(request.path)
    app.logger.info(request.method)
    app.logger.info(str(request.get_data()))
    app.logger.info(str(request.get_json()))

    return Response(status=HTTPStatus.OK)

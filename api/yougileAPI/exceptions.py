# coding: utf-8

"""
    YouGile REST API v2.0

    ## Введение  Новая версия REST API от YouGile обладает значительно более широким функционалом по сравнению с первой — существующие запросы получили дополнительные возможности, а также был добавлен ряд новых запросов, которых не было ранее. Вторая версия API несовместима с первой, это нужно учесть тем пользователям, которые хотят перейти с предыдущей версии.  Реализованные на текущий момент запросы можно найти в меню слева, в разделе ENDPOINTS. На данный момент работа над API продолжается. Ниже на этой странице перечислен функционал, который будет добавлен в ближайшее время.  Любые пожелания по развитию функциональности API обязательно сообщайте нам — support@yougile.com  ## Как использовать  operations/AuthKeyController_companiesList  - [Получить](operations/AuthKeyController_companiesList) ID компании, которой хочется управлять с помощью REST API (для этого нужен логин и пароль от аккаунта в YouGile). - [Получить](operations/AuthKeyController_create) ключ API (для этого нужен логин и пароль от аккаунта в YouGile и ID компании). - Использовать этот ключ для дальнейших запросов к API. - Ключ API не имеет ограничений по времени и по количеству запросов с его использованием. Есть только ограничение на их частоту - не более 30 в минуту на компанию. - API позволяет управлять ключами: получать список и удалять. У каждого аккаунта может быть не более 30 ключей. - Запрос к API – это HTTPS запрос к адресу вида:   ```http   https://yougile.com/api-v2/{resource}   ```   (http метод GET, POST, PUT или DELETE) - К каждому запросу нужно добавлять заголовки: `Content-Type: application/json` и `Authorization: Bearer <ключ API>`. - Параметры запроса передаются в виде JSON в теле запроса. - Если запрос был успешно выполнен, то ответ будет иметь статус `200` или `201` (в случае создания новых объектов), если же запрос был неуспешным, статус будет начинаться с `3`, `4` или `5` в зависимости от типа ошибки. Также будет присутствовать поле `error` с описанием ошибки. - API при работе проверяет и использует права аккаунта / владельца ключа. Если сотрудники не могут что-то увидеть или сделать через интерфейс из-за недостаточных прав, то же самое ограничение они будут иметь и через API.  ---  Поддержка, консультации, обратная связь — support@yougile.com   # noqa: E501

    The version of the OpenAPI document: 2.0
    Generated by: https://openapi-generator.tech
"""


import six


class OpenApiException(Exception):
    """The base exception class for all OpenAPIExceptions"""


class ApiTypeError(OpenApiException, TypeError):
    def __init__(self, msg, path_to_item=None, valid_classes=None,
                 key_type=None):
        """ Raises an exception for TypeErrors

        Args:
            msg (str): the exception message

        Keyword Args:
            path_to_item (list): a list of keys an indices to get to the
                                 current_item
                                 None if unset
            valid_classes (tuple): the primitive classes that current item
                                   should be an instance of
                                   None if unset
            key_type (bool): False if our value is a value in a dict
                             True if it is a key in a dict
                             False if our item is an item in a list
                             None if unset
        """
        self.path_to_item = path_to_item
        self.valid_classes = valid_classes
        self.key_type = key_type
        full_msg = msg
        if path_to_item:
            full_msg = "{0} at {1}".format(msg, render_path(path_to_item))
        super(ApiTypeError, self).__init__(full_msg)


class ApiValueError(OpenApiException, ValueError):
    def __init__(self, msg, path_to_item=None):
        """
        Args:
            msg (str): the exception message

        Keyword Args:
            path_to_item (list) the path to the exception in the
                received_data dict. None if unset
        """

        self.path_to_item = path_to_item
        full_msg = msg
        if path_to_item:
            full_msg = "{0} at {1}".format(msg, render_path(path_to_item))
        super(ApiValueError, self).__init__(full_msg)


class ApiKeyError(OpenApiException, KeyError):
    def __init__(self, msg, path_to_item=None):
        """
        Args:
            msg (str): the exception message

        Keyword Args:
            path_to_item (None/list) the path to the exception in the
                received_data dict
        """
        self.path_to_item = path_to_item
        full_msg = msg
        if path_to_item:
            full_msg = "{0} at {1}".format(msg, render_path(path_to_item))
        super(ApiKeyError, self).__init__(full_msg)


class ApiException(OpenApiException):

    def __init__(self, status=None, reason=None, http_resp=None):
        if http_resp:
            self.status = http_resp.status
            self.reason = http_resp.reason
            self.body = http_resp.data
            self.headers = http_resp.getheaders()
        else:
            self.status = status
            self.reason = reason
            self.body = None
            self.headers = None

    def __str__(self):
        """Custom error messages for exception"""
        error_message = "({0})\n"\
                        "Reason: {1}\n".format(self.status, self.reason)
        if self.headers:
            error_message += "HTTP response headers: {0}\n".format(
                self.headers)

        if self.body:
            error_message += "HTTP response body: {0}\n".format(self.body)

        return error_message


def render_path(path_to_item):
    """Returns a string representation of a path"""
    result = ""
    for pth in path_to_item:
        if isinstance(pth, six.integer_types):
            result += "[{0}]".format(pth)
        else:
            result += "['{0}']".format(pth)
    return result

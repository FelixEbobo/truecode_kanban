# coding: utf-8

"""
    YouGile REST API v2.0

    ## Введение  Новая версия REST API от YouGile обладает значительно более широким функционалом по сравнению с первой — существующие запросы получили дополнительные возможности, а также был добавлен ряд новых запросов, которых не было ранее. Вторая версия API несовместима с первой, это нужно учесть тем пользователям, которые хотят перейти с предыдущей версии.  Реализованные на текущий момент запросы можно найти в меню слева, в разделе ENDPOINTS. На данный момент работа над API продолжается. Ниже на этой странице перечислен функционал, который будет добавлен в ближайшее время.  Любые пожелания по развитию функциональности API обязательно сообщайте нам — support@yougile.com  ## Как использовать  operations/AuthKeyController_companiesList  - [Получить](operations/AuthKeyController_companiesList) ID компании, которой хочется управлять с помощью REST API (для этого нужен логин и пароль от аккаунта в YouGile). - [Получить](operations/AuthKeyController_create) ключ API (для этого нужен логин и пароль от аккаунта в YouGile и ID компании). - Использовать этот ключ для дальнейших запросов к API. - Ключ API не имеет ограничений по времени и по количеству запросов с его использованием. Есть только ограничение на их частоту - не более 30 в минуту на компанию. - API позволяет управлять ключами: получать список и удалять. У каждого аккаунта может быть не более 30 ключей. - Запрос к API – это HTTPS запрос к адресу вида:   ```http   https://yougile.com/api-v2/{resource}   ```   (http метод GET, POST, PUT или DELETE) - К каждому запросу нужно добавлять заголовки: `Content-Type: application/json` и `Authorization: Bearer <ключ API>`. - Параметры запроса передаются в виде JSON в теле запроса. - Если запрос был успешно выполнен, то ответ будет иметь статус `200` или `201` (в случае создания новых объектов), если же запрос был неуспешным, статус будет начинаться с `3`, `4` или `5` в зависимости от типа ошибки. Также будет присутствовать поле `error` с описанием ошибки. - API при работе проверяет и использует права аккаунта / владельца ключа. Если сотрудники не могут что-то увидеть или сделать через интерфейс из-за недостаточных прав, то же самое ограничение они будут иметь и через API.  ---  Поддержка, консультации, обратная связь — support@yougile.com   # noqa: E501

    The version of the OpenAPI document: 2.0
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import yougileAPI
from yougileAPI.models.board_permissions_dto import BoardPermissionsDto  # noqa: E501
from yougileAPI.rest import ApiException

class TestBoardPermissionsDto(unittest.TestCase):
    """BoardPermissionsDto unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test BoardPermissionsDto
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # model = yougileAPI.models.board_permissions_dto.BoardPermissionsDto()  # noqa: E501
        if include_optional :
            return BoardPermissionsDto(
                edit_title = True, 
                delete = True, 
                move = True, 
                show_stickers = True, 
                edit_stickers = True, 
                add_column = True, 
                columns = yougileAPI.models.column_permissions_dto.ColumnPermissionsDto(
                    edit_title = True, 
                    delete = True, 
                    move = 'no', 
                    add_task = True, 
                    all_tasks = yougileAPI.models.task_permissions_dto.TaskPermissionsDto(
                        show = True, 
                        delete = True, 
                        edit_title = True, 
                        edit_description = True, 
                        complete = True, 
                        close = True, 
                        assign_users = 'no', 
                        connect = True, 
                        edit_subtasks = 'no', 
                        edit_stickers = True, 
                        edit_pins = True, 
                        move = 'no', 
                        send_messages = True, 
                        send_files = True, 
                        edit_who_to_notify = 'no', ), 
                    with_me_tasks = yougileAPI.models.task_permissions_dto.TaskPermissionsDto(
                        show = True, 
                        delete = True, 
                        edit_title = True, 
                        edit_description = True, 
                        complete = True, 
                        close = True, 
                        assign_users = 'no', 
                        connect = True, 
                        edit_subtasks = 'no', 
                        edit_stickers = True, 
                        edit_pins = True, 
                        move = 'no', 
                        send_messages = True, 
                        send_files = True, 
                        edit_who_to_notify = 'no', ), 
                    my_tasks = yougileAPI.models.task_permissions_dto.TaskPermissionsDto(
                        show = True, 
                        delete = True, 
                        edit_title = True, 
                        edit_description = True, 
                        complete = True, 
                        close = True, 
                        assign_users = 'no', 
                        connect = True, 
                        edit_subtasks = 'no', 
                        edit_stickers = True, 
                        edit_pins = True, 
                        move = 'no', 
                        send_messages = True, 
                        send_files = True, 
                        edit_who_to_notify = 'no', ), 
                    created_by_me_tasks = yougileAPI.models.task_permissions_dto.TaskPermissionsDto(
                        show = True, 
                        delete = True, 
                        edit_title = True, 
                        edit_description = True, 
                        complete = True, 
                        close = True, 
                        assign_users = 'no', 
                        connect = True, 
                        edit_subtasks = 'no', 
                        edit_stickers = True, 
                        edit_pins = True, 
                        move = 'no', 
                        send_messages = True, 
                        send_files = True, 
                        edit_who_to_notify = 'no', ), ), 
                settings = True
            )
        else :
            return BoardPermissionsDto(
                edit_title = True,
                delete = True,
                move = True,
                show_stickers = True,
                edit_stickers = True,
                add_column = True,
                columns = yougileAPI.models.column_permissions_dto.ColumnPermissionsDto(
                    edit_title = True, 
                    delete = True, 
                    move = 'no', 
                    add_task = True, 
                    all_tasks = yougileAPI.models.task_permissions_dto.TaskPermissionsDto(
                        show = True, 
                        delete = True, 
                        edit_title = True, 
                        edit_description = True, 
                        complete = True, 
                        close = True, 
                        assign_users = 'no', 
                        connect = True, 
                        edit_subtasks = 'no', 
                        edit_stickers = True, 
                        edit_pins = True, 
                        move = 'no', 
                        send_messages = True, 
                        send_files = True, 
                        edit_who_to_notify = 'no', ), 
                    with_me_tasks = yougileAPI.models.task_permissions_dto.TaskPermissionsDto(
                        show = True, 
                        delete = True, 
                        edit_title = True, 
                        edit_description = True, 
                        complete = True, 
                        close = True, 
                        assign_users = 'no', 
                        connect = True, 
                        edit_subtasks = 'no', 
                        edit_stickers = True, 
                        edit_pins = True, 
                        move = 'no', 
                        send_messages = True, 
                        send_files = True, 
                        edit_who_to_notify = 'no', ), 
                    my_tasks = yougileAPI.models.task_permissions_dto.TaskPermissionsDto(
                        show = True, 
                        delete = True, 
                        edit_title = True, 
                        edit_description = True, 
                        complete = True, 
                        close = True, 
                        assign_users = 'no', 
                        connect = True, 
                        edit_subtasks = 'no', 
                        edit_stickers = True, 
                        edit_pins = True, 
                        move = 'no', 
                        send_messages = True, 
                        send_files = True, 
                        edit_who_to_notify = 'no', ), 
                    created_by_me_tasks = yougileAPI.models.task_permissions_dto.TaskPermissionsDto(
                        show = True, 
                        delete = True, 
                        edit_title = True, 
                        edit_description = True, 
                        complete = True, 
                        close = True, 
                        assign_users = 'no', 
                        connect = True, 
                        edit_subtasks = 'no', 
                        edit_stickers = True, 
                        edit_pins = True, 
                        move = 'no', 
                        send_messages = True, 
                        send_files = True, 
                        edit_who_to_notify = 'no', ), ),
                settings = True,
        )

    def testBoardPermissionsDto(self):
        """Test BoardPermissionsDto"""
        inst_req_only = self.make_instance(include_optional=False)
        inst_req_and_optional = self.make_instance(include_optional=True)


if __name__ == '__main__':
    unittest.main()

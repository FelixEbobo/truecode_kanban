# yougileAPI.GroupChatsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**group_chat_controller_create**](GroupChatsApi.md#group_chat_controller_create) | **POST** /api-v2/group-chats | Создать чат
[**group_chat_controller_get**](GroupChatsApi.md#group_chat_controller_get) | **GET** /api-v2/group-chats/{id} | Получить чат по ID
[**group_chat_controller_search**](GroupChatsApi.md#group_chat_controller_search) | **GET** /api-v2/group-chats | Получить список чатов
[**group_chat_controller_update**](GroupChatsApi.md#group_chat_controller_update) | **PUT** /api-v2/group-chats/{id} | Изменить чат


# **group_chat_controller_create**
> WithIdDto group_chat_controller_create(create_group_chat_dto)

Создать чат

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint
configuration = yougileAPI.Configuration()
# Configure Bearer authorization (JWT): bearer
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to http://localhost
configuration.host = "http://localhost"

# Enter a context with an instance of the API client
with yougileAPI.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.GroupChatsApi(api_client)
    create_group_chat_dto = yougileAPI.CreateGroupChatDto() # CreateGroupChatDto | 

    try:
        # Создать чат
        api_response = api_instance.group_chat_controller_create(create_group_chat_dto)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling GroupChatsApi->group_chat_controller_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_group_chat_dto** | [**CreateGroupChatDto**](CreateGroupChatDto.md)|  | 

### Return type

[**WithIdDto**](WithIdDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **group_chat_controller_get**
> GroupChatDto group_chat_controller_get(id)

Получить чат по ID

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint
configuration = yougileAPI.Configuration()
# Configure Bearer authorization (JWT): bearer
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to http://localhost
configuration.host = "http://localhost"

# Enter a context with an instance of the API client
with yougileAPI.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.GroupChatsApi(api_client)
    id = 'id_example' # str | 

    try:
        # Получить чат по ID
        api_response = api_instance.group_chat_controller_get(id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling GroupChatsApi->group_chat_controller_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 

### Return type

[**GroupChatDto**](GroupChatDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**404** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **group_chat_controller_search**
> GroupChatListDto group_chat_controller_search(include_deleted=include_deleted, limit=limit, offset=offset, title=title)

Получить список чатов

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint
configuration = yougileAPI.Configuration()
# Configure Bearer authorization (JWT): bearer
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to http://localhost
configuration.host = "http://localhost"

# Enter a context with an instance of the API client
with yougileAPI.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.GroupChatsApi(api_client)
    include_deleted = True # bool | По умолчанию, если объект был отмечен как удаленный, то он не будет найден.    Поставьте true, чтобы удаленные объекты возвращались. (optional)
limit = 50 # float | Количество элементов, которые хочется получить. Максимум 1000. (optional) (default to 50)
offset = 0 # float | Индекс первого элемента страницы (optional) (default to 0)
title = 'title_example' # str | Имя чата (optional)

    try:
        # Получить список чатов
        api_response = api_instance.group_chat_controller_search(include_deleted=include_deleted, limit=limit, offset=offset, title=title)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling GroupChatsApi->group_chat_controller_search: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **include_deleted** | **bool**| По умолчанию, если объект был отмечен как удаленный, то он не будет найден.    Поставьте true, чтобы удаленные объекты возвращались. | [optional] 
 **limit** | **float**| Количество элементов, которые хочется получить. Максимум 1000. | [optional] [default to 50]
 **offset** | **float**| Индекс первого элемента страницы | [optional] [default to 0]
 **title** | **str**| Имя чата | [optional] 

### Return type

[**GroupChatListDto**](GroupChatListDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**404** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **group_chat_controller_update**
> WithIdDto group_chat_controller_update(id, update_group_chat_dto)

Изменить чат

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint
configuration = yougileAPI.Configuration()
# Configure Bearer authorization (JWT): bearer
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to http://localhost
configuration.host = "http://localhost"

# Enter a context with an instance of the API client
with yougileAPI.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.GroupChatsApi(api_client)
    id = 'id_example' # str | 
update_group_chat_dto = yougileAPI.UpdateGroupChatDto() # UpdateGroupChatDto | 

    try:
        # Изменить чат
        api_response = api_instance.group_chat_controller_update(id, update_group_chat_dto)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling GroupChatsApi->group_chat_controller_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **update_group_chat_dto** | [**UpdateGroupChatDto**](UpdateGroupChatDto.md)|  | 

### Return type

[**WithIdDto**](WithIdDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**404** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


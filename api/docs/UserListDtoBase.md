# UserListDtoBase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | ID объекта | 
**email** | **str** | Почтовый ящик сотрудника | 
**is_admin** | **bool** | Имеет ли пользователь права администратора | [optional] 
**real_name** | **str** | ФИО | 
**status** | **str** | Статус online/offline | 
**last_activity** | **float** | Время последнего действия в системе | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# CredentialsWithCompanyOptionalDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**login** | **str** | Логин пользователя | [optional] 
**password** | **str** | Пароль пользователя | [optional] 
**company_id** | **str** | ID компании | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



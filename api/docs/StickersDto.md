# StickersDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timer** | **bool** | Таймер | [optional] 
**deadline** | **bool** | Дедлайн | [optional] 
**stopwatch** | **bool** | Секундомер | [optional] 
**time_tracking** | **bool** | Таймтрекинг | [optional] 
**assignee** | **bool** | Исполнитель | [optional] 
**repeat** | **bool** | Регулярная задача | [optional] 
**custom** | [**object**](.md) | Пользовательские стикеры доски | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



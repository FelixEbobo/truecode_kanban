# UpdateUserDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**is_admin** | **bool** | Имеет ли пользователь права администратора | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# ProjectRoleListDtoBase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | ID объекта | 
**name** | **str** | Название роли | 
**description** | **str** | Описание роли | [optional] 
**permissions** | [**ProjectPermissionsDto**](ProjectPermissionsDto.md) | Права в проекте | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



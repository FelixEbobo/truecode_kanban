# StringStickerWithStatesListDtoBase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | ID объекта | 
**deleted** | **bool** | Если true, значит объект удален | [optional] 
**name** | **str** | Имя стикера | 
**icon** | **str** | Иконка стикера | [optional] 
**states** | [**list[StringStickerStateDto]**](StringStickerStateDto.md) | Состояния стикера. | [optional] 
**limit** | **float** | Количество элементов, которые хочется получить. Максимум 1000. | [optional] [default to 50]
**offset** | **float** | Индекс первого элемента страницы | [optional] [default to 0]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



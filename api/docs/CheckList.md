# CheckList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **str** | Название списка чеклистов | 
**items** | [**CheckListItem**](CheckListItem.md) | Массив с чеклистами | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



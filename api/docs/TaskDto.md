# TaskDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | ID объекта | 
**deleted** | **bool** | Если true, значит объект удален | [optional] 
**title** | **str** | Название задачи | 
**timestamp** | **float** | Время создания задачи | 
**column_id** | **str** | Id колонки родителя | [optional] 
**description** | **str** | Описание задачи | [optional] 
**archived** | **bool** | Задача перенесена в архив - да/нет | [optional] 
**completed** | **bool** | Задача выполнена - да/нет | [optional] 
**subtasks** | **list[str]** | Массив Id подзадач | [optional] 
**assigned** | **list[str]** | Массив Id пользователей, на которых назначена задача | [optional] 
**created_by** | **str** | Id пользователя, который создал задачу | [optional] 
**deadline** | [**Deadline**](Deadline.md) | Стикер \&quot;Дэдлайн\&quot;. Указывает на крайний срок выполнения задачи. Имеется возможность кроме даты указать время, а так же дату начала задачи. | [optional] 
**time_tracking** | [**TimeTracking**](TimeTracking.md) | Стикер \&quot;Таймтрекинг\&quot;. Используется для указания ожидаемого и реального времени на выполнение задачи. | [optional] 
**checklists** | [**list[CheckList]**](CheckList.md) | Чеклисты. К задаче всегда будет присвоен переданный объект. Если необходимо внести изменения, нужно сначала получить чеклисты, затем произвести корректировку, а затем записать в задачу заново. | [optional] 
**stickers** | [**object**](.md) | Пользовательские стикеры. Передаются в виде объекта ключ-значение. Где ключ - это ID стикера, значение - ID состояния. Для открепления стикера от задачи, используйте \&quot;-\&quot; как значение состояния | [optional] 
**stopwatch** | [**Stopwatch**](Stopwatch.md) |  | [optional] 
**timer** | [**Timer**](Timer.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



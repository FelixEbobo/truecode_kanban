# BoardPermissionsDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**edit_title** | **bool** |  | 
**delete** | **bool** |  | 
**move** | **bool** |  | 
**show_stickers** | **bool** |  | 
**edit_stickers** | **bool** |  | 
**add_column** | **bool** |  | 
**columns** | [**ColumnPermissionsDto**](ColumnPermissionsDto.md) |  | 
**settings** | **bool** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# ProjectPermissionsDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**edit_title** | **bool** |  | 
**delete** | **bool** |  | 
**add_board** | **bool** |  | 
**boards** | [**BoardPermissionsDto**](BoardPermissionsDto.md) |  | 
**children** | [**object**](.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



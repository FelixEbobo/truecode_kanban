# yougileAPI.TasksApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**task_controller_create**](TasksApi.md#task_controller_create) | **POST** /api-v2/tasks | Создать
[**task_controller_get**](TasksApi.md#task_controller_get) | **GET** /api-v2/tasks/{id} | Получить по ID
[**task_controller_search**](TasksApi.md#task_controller_search) | **GET** /api-v2/tasks | Получить список
[**task_controller_update**](TasksApi.md#task_controller_update) | **PUT** /api-v2/tasks/{id} | Изменить


# **task_controller_create**
> WithIdDto task_controller_create(create_task_dto)

Создать

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint
configuration = yougileAPI.Configuration()
# Configure Bearer authorization (JWT): bearer
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to http://localhost
configuration.host = "http://localhost"

# Enter a context with an instance of the API client
with yougileAPI.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.TasksApi(api_client)
    create_task_dto = yougileAPI.CreateTaskDto() # CreateTaskDto | 

    try:
        # Создать
        api_response = api_instance.task_controller_create(create_task_dto)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling TasksApi->task_controller_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_task_dto** | [**CreateTaskDto**](CreateTaskDto.md)|  | 

### Return type

[**WithIdDto**](WithIdDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Задача успешно создана. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **task_controller_get**
> TaskDto task_controller_get(id)

Получить по ID

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint
configuration = yougileAPI.Configuration()
# Configure Bearer authorization (JWT): bearer
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to http://localhost
configuration.host = "http://localhost"

# Enter a context with an instance of the API client
with yougileAPI.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.TasksApi(api_client)
    id = 'id_example' # str | 

    try:
        # Получить по ID
        api_response = api_instance.task_controller_get(id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling TasksApi->task_controller_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 

### Return type

[**TaskDto**](TaskDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**404** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **task_controller_search**
> TaskListDto task_controller_search(include_deleted=include_deleted, limit=limit, offset=offset, title=title, column_id=column_id)

Получить список

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint
configuration = yougileAPI.Configuration()
# Configure Bearer authorization (JWT): bearer
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to http://localhost
configuration.host = "http://localhost"

# Enter a context with an instance of the API client
with yougileAPI.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.TasksApi(api_client)
    include_deleted = True # bool | По умолчанию, если объект был отмечен как удаленный, то он не будет найден.    Поставьте true, чтобы удаленные объекты возвращались. (optional)
limit = 50 # float | Количество элементов, которые хочется получить. Максимум 1000. (optional) (default to 50)
offset = 0 # float | Индекс первого элемента страницы (optional) (default to 0)
title = 'title_example' # str | Заголовок задачи (optional)
column_id = 'column_id_example' # str | ID колонки (optional)

    try:
        # Получить список
        api_response = api_instance.task_controller_search(include_deleted=include_deleted, limit=limit, offset=offset, title=title, column_id=column_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling TasksApi->task_controller_search: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **include_deleted** | **bool**| По умолчанию, если объект был отмечен как удаленный, то он не будет найден.    Поставьте true, чтобы удаленные объекты возвращались. | [optional] 
 **limit** | **float**| Количество элементов, которые хочется получить. Максимум 1000. | [optional] [default to 50]
 **offset** | **float**| Индекс первого элемента страницы | [optional] [default to 0]
 **title** | **str**| Заголовок задачи | [optional] 
 **column_id** | **str**| ID колонки | [optional] 

### Return type

[**TaskListDto**](TaskListDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**404** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **task_controller_update**
> WithIdDto task_controller_update(id, update_task_dto)

Изменить

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint
configuration = yougileAPI.Configuration()
# Configure Bearer authorization (JWT): bearer
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to http://localhost
configuration.host = "http://localhost"

# Enter a context with an instance of the API client
with yougileAPI.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.TasksApi(api_client)
    id = 'id_example' # str | 
update_task_dto = yougileAPI.UpdateTaskDto() # UpdateTaskDto | 

    try:
        # Изменить
        api_response = api_instance.task_controller_update(id, update_task_dto)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling TasksApi->task_controller_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **update_task_dto** | [**UpdateTaskDto**](UpdateTaskDto.md)|  | 

### Return type

[**WithIdDto**](WithIdDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Задача успешно обновлена. |  -  |
**404** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


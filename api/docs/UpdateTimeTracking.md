# UpdateTimeTracking

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**plan** | **float** | Сколько часов было запланировано на выполнение задачи | [optional] 
**work** | **float** | Сколько часов было затрачено на выполнение задачи | [optional] 
**deleted** | **bool** | Открепить стикер от задачи (true) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



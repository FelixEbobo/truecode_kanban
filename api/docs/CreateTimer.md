# CreateTimer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**seconds** | **float** | Установить время таймера в секундах. | 
**running** | **bool** | Запустить или остановить таймер. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



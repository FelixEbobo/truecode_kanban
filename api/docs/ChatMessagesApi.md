# yougileAPI.ChatMessagesApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**chat_message_controller_search**](ChatMessagesApi.md#chat_message_controller_search) | **GET** /api-v2/chats/{chatId}/messages | Получить историю сообщений
[**chat_message_controller_send_message**](ChatMessagesApi.md#chat_message_controller_send_message) | **POST** /api-v2/chats/{chatId}/messages | Написать в чат


# **chat_message_controller_search**
> ChatMessageListDto chat_message_controller_search(chat_id, include_deleted=include_deleted, limit=limit, offset=offset, from_user_id=from_user_id, text=text, label=label, since=since, include_system=include_system)

Получить историю сообщений

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint
configuration = yougileAPI.Configuration()
# Configure Bearer authorization (JWT): bearer
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to http://localhost
configuration.host = "http://localhost"

# Enter a context with an instance of the API client
with yougileAPI.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.ChatMessagesApi(api_client)
    chat_id = 'chat_id_example' # str | 
include_deleted = True # bool | По умолчанию, если объект был отмечен как удаленный, то он не будет найден.    Поставьте true, чтобы удаленные объекты возвращались. (optional)
limit = 50 # float | Количество элементов, которые хочется получить. Максимум 1000. (optional) (default to 50)
offset = 0 # float | Индекс первого элемента страницы (optional) (default to 0)
from_user_id = 'from_user_id_example' # str | ID сотрудника от кого сообщение (optional)
text = 'text_example' # str | Строка, которую сообщение должно содержать (optional)
label = 'label_example' # str | Поиск по быстрой ссылке сообщения (optional)
since = 3.4 # float | Искать среди сообщений, время создания которых позже указанного времени (timestamp) (optional)
include_system = True # bool | Включать ли системные сообщения. По умолчанию они не включаются. (optional)

    try:
        # Получить историю сообщений
        api_response = api_instance.chat_message_controller_search(chat_id, include_deleted=include_deleted, limit=limit, offset=offset, from_user_id=from_user_id, text=text, label=label, since=since, include_system=include_system)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ChatMessagesApi->chat_message_controller_search: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **chat_id** | **str**|  | 
 **include_deleted** | **bool**| По умолчанию, если объект был отмечен как удаленный, то он не будет найден.    Поставьте true, чтобы удаленные объекты возвращались. | [optional] 
 **limit** | **float**| Количество элементов, которые хочется получить. Максимум 1000. | [optional] [default to 50]
 **offset** | **float**| Индекс первого элемента страницы | [optional] [default to 0]
 **from_user_id** | **str**| ID сотрудника от кого сообщение | [optional] 
 **text** | **str**| Строка, которую сообщение должно содержать | [optional] 
 **label** | **str**| Поиск по быстрой ссылке сообщения | [optional] 
 **since** | **float**| Искать среди сообщений, время создания которых позже указанного времени (timestamp) | [optional] 
 **include_system** | **bool**| Включать ли системные сообщения. По умолчанию они не включаются. | [optional] 

### Return type

[**ChatMessageListDto**](ChatMessageListDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**404** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **chat_message_controller_send_message**
> ChatIdDto chat_message_controller_send_message(chat_id, create_chat_message_dto)

Написать в чат

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint
configuration = yougileAPI.Configuration()
# Configure Bearer authorization (JWT): bearer
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to http://localhost
configuration.host = "http://localhost"

# Enter a context with an instance of the API client
with yougileAPI.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.ChatMessagesApi(api_client)
    chat_id = 'chat_id_example' # str | 
create_chat_message_dto = yougileAPI.CreateChatMessageDto() # CreateChatMessageDto | 

    try:
        # Написать в чат
        api_response = api_instance.chat_message_controller_send_message(chat_id, create_chat_message_dto)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ChatMessagesApi->chat_message_controller_send_message: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **chat_id** | **str**|  | 
 **create_chat_message_dto** | [**CreateChatMessageDto**](CreateChatMessageDto.md)|  | 

### Return type

[**ChatIdDto**](ChatIdDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


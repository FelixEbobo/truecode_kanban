# yougileAPI.SprintStickerApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sprint_sticker_controller_create**](SprintStickerApi.md#sprint_sticker_controller_create) | **POST** /api-v2/sprint-stickers | Создать
[**sprint_sticker_controller_get_sticker**](SprintStickerApi.md#sprint_sticker_controller_get_sticker) | **GET** /api-v2/sprint-stickers/{id} | Получить по ID
[**sprint_sticker_controller_search**](SprintStickerApi.md#sprint_sticker_controller_search) | **GET** /api-v2/sprint-stickers | Получить список
[**sprint_sticker_controller_update**](SprintStickerApi.md#sprint_sticker_controller_update) | **PUT** /api-v2/sprint-stickers/{id} | Изменить


# **sprint_sticker_controller_create**
> WithIdDto sprint_sticker_controller_create(create_sprint_sticker_dto)

Создать

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint
configuration = yougileAPI.Configuration()
# Configure Bearer authorization (JWT): bearer
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to http://localhost
configuration.host = "http://localhost"

# Enter a context with an instance of the API client
with yougileAPI.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.SprintStickerApi(api_client)
    create_sprint_sticker_dto = yougileAPI.CreateSprintStickerDto() # CreateSprintStickerDto | 

    try:
        # Создать
        api_response = api_instance.sprint_sticker_controller_create(create_sprint_sticker_dto)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling SprintStickerApi->sprint_sticker_controller_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_sprint_sticker_dto** | [**CreateSprintStickerDto**](CreateSprintStickerDto.md)|  | 

### Return type

[**WithIdDto**](WithIdDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **sprint_sticker_controller_get_sticker**
> SprintStickerWithStatesDto sprint_sticker_controller_get_sticker(id)

Получить по ID

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint
configuration = yougileAPI.Configuration()
# Configure Bearer authorization (JWT): bearer
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to http://localhost
configuration.host = "http://localhost"

# Enter a context with an instance of the API client
with yougileAPI.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.SprintStickerApi(api_client)
    id = 'id_example' # str | 

    try:
        # Получить по ID
        api_response = api_instance.sprint_sticker_controller_get_sticker(id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling SprintStickerApi->sprint_sticker_controller_get_sticker: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 

### Return type

[**SprintStickerWithStatesDto**](SprintStickerWithStatesDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**404** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **sprint_sticker_controller_search**
> SprintStickerWithStatesListDto sprint_sticker_controller_search(include_deleted=include_deleted, limit=limit, offset=offset, name=name, board_id=board_id)

Получить список

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint
configuration = yougileAPI.Configuration()
# Configure Bearer authorization (JWT): bearer
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to http://localhost
configuration.host = "http://localhost"

# Enter a context with an instance of the API client
with yougileAPI.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.SprintStickerApi(api_client)
    include_deleted = True # bool | По умолчанию, если объект был отмечен как удаленный, то он не будет найден.    Поставьте true, чтобы удаленные объекты возвращались. (optional)
limit = 50 # float | Количество элементов, которые хочется получить. Максимум 1000. (optional) (default to 50)
offset = 0 # float | Индекс первого элемента страницы (optional) (default to 0)
name = 'name_example' # str | Имя стикера (optional)
board_id = 'board_id_example' # str | ID доски (optional)

    try:
        # Получить список
        api_response = api_instance.sprint_sticker_controller_search(include_deleted=include_deleted, limit=limit, offset=offset, name=name, board_id=board_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling SprintStickerApi->sprint_sticker_controller_search: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **include_deleted** | **bool**| По умолчанию, если объект был отмечен как удаленный, то он не будет найден.    Поставьте true, чтобы удаленные объекты возвращались. | [optional] 
 **limit** | **float**| Количество элементов, которые хочется получить. Максимум 1000. | [optional] [default to 50]
 **offset** | **float**| Индекс первого элемента страницы | [optional] [default to 0]
 **name** | **str**| Имя стикера | [optional] 
 **board_id** | **str**| ID доски | [optional] 

### Return type

[**SprintStickerWithStatesListDto**](SprintStickerWithStatesListDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**404** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **sprint_sticker_controller_update**
> WithIdDto sprint_sticker_controller_update(id, update_sprint_sticker_dto)

Изменить

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint
configuration = yougileAPI.Configuration()
# Configure Bearer authorization (JWT): bearer
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to http://localhost
configuration.host = "http://localhost"

# Enter a context with an instance of the API client
with yougileAPI.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.SprintStickerApi(api_client)
    id = 'id_example' # str | 
update_sprint_sticker_dto = yougileAPI.UpdateSprintStickerDto() # UpdateSprintStickerDto | 

    try:
        # Изменить
        api_response = api_instance.sprint_sticker_controller_update(id, update_sprint_sticker_dto)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling SprintStickerApi->sprint_sticker_controller_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **update_sprint_sticker_dto** | [**UpdateSprintStickerDto**](UpdateSprintStickerDto.md)|  | 

### Return type

[**WithIdDto**](WithIdDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**404** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


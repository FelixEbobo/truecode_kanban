# UpdateTimer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**seconds** | **float** | Установить время таймера в секундах. | [optional] 
**running** | **bool** | Запустить или остановить таймер. | [optional] 
**deleted** | **bool** | Открепить стикер от задачи (true) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



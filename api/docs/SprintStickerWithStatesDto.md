# SprintStickerWithStatesDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | ID объекта | 
**deleted** | **bool** | Если true, значит объект удален | [optional] 
**name** | **str** | Имя стикера | 
**states** | [**list[SprintStickerStateDto]**](SprintStickerStateDto.md) | Состояния стикера. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# CreateBoardDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **str** | Название доски | 
**project_id** | **str** | Id проекта, в котором находится доска | 
**stickers** | [**StickersDto**](StickersDto.md) | Стикеры доски | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# ColumnListDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**paging** | [**PagingMetadata**](PagingMetadata.md) | Дополнительная информация о странице | 
**content** | [**list[ColumnListDtoBase]**](ColumnListDtoBase.md) | Список колонок | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



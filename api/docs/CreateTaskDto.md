# CreateTaskDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **str** | Название задачи | 
**column_id** | **str** | Id колонки родителя | [optional] 
**description** | **str** | Описание задачи | [optional] 
**archived** | **bool** | Задача перенесена в архив - да/нет | [optional] 
**completed** | **bool** | Задача выполнена - да/нет | [optional] 
**subtasks** | **list[str]** | Массив Id подзадач | [optional] 
**assigned** | **list[str]** | Массив Id пользователей, на которых назначена задача | [optional] 
**deadline** | [**Deadline**](Deadline.md) | Стикер \&quot;Дэдлайн\&quot;. Указывает на крайний срок выполнения задачи. Имеется возможность кроме даты указать время, а так же дату начала задачи. | [optional] 
**time_tracking** | [**TimeTracking**](TimeTracking.md) | Стикер \&quot;Таймтрекинг\&quot;. Используется для указания ожидаемого и реального времени на выполнение задачи. | [optional] 
**checklists** | [**list[CheckList]**](CheckList.md) | Чеклисты. К задаче всегда будет присвоен переданный объект. Если необходимо внести изменения, нужно сначала получить чеклисты, затем произвести корректировку, а затем записать в задачу заново. | [optional] 
**stickers** | [**object**](.md) | Пользовательские стикеры. Передаются в виде объекта ключ-значение. Где ключ - это ID стикера, значение - ID состояния. Для открепления стикера от задачи, используйте \&quot;-\&quot; как значение состояния | [optional] 
**stopwatch** | [**CreateStopwatch**](CreateStopwatch.md) | Стикер \&quot;Секундомер\&quot;. Позволяет запустить секундомер, а так же ставить его на паузу и запускать заново. | [optional] 
**timer** | [**CreateTimer**](CreateTimer.md) | Стикер \&quot;Таймер\&quot;. Позволяет установить таймер на заданное время, а также возможность ставить его на паузу и запускать заново | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



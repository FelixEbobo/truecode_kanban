# UpdateWebhookDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deleted** | **bool** | Если true, значит объект удален | [optional] 
**url** | **str** |  | [optional] 
**event** | **str** | Событие подписки. Формат: &#x60;&lt;тип_объекта&gt;-&lt;событие&gt;&#x60;. Для объектов &#x60;project,board,column,task,sticker,department,group_chat,chat_message&#x60;,  возможные события: &#x60;created,deleted,restored,moved,renamed&#x60;. Для объектов &#x60;user&#x60;, возможные события: &#x60;added&#x60;, &#x60;removed&#x60;.  Может использоваться javascript regexp как значение. Например, &#x60;task-*&#x60; - подписка на все события по задачам,  или &#x60;.*&#x60; - подписка на все события. | [optional] 
**disabled** | **bool** | Если true, то вызываться не будет | [optional] [default to False]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



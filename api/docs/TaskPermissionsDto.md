# TaskPermissionsDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**show** | **bool** |  | 
**delete** | **bool** |  | 
**edit_title** | **bool** |  | 
**edit_description** | **bool** |  | 
**complete** | **bool** |  | 
**close** | **bool** |  | 
**assign_users** | **str** |  | [default to 'no']
**connect** | **bool** |  | 
**edit_subtasks** | **str** |  | [default to 'no']
**edit_stickers** | **bool** |  | 
**edit_pins** | **bool** |  | 
**move** | **str** |  | [default to 'no']
**send_messages** | **bool** |  | 
**send_files** | **bool** |  | 
**edit_who_to_notify** | **str** |  | [default to 'no']

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



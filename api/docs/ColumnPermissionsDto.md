# ColumnPermissionsDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**edit_title** | **bool** |  | 
**delete** | **bool** |  | 
**move** | **str** |  | [default to 'no']
**add_task** | **bool** |  | 
**all_tasks** | [**TaskPermissionsDto**](TaskPermissionsDto.md) |  | 
**with_me_tasks** | [**TaskPermissionsDto**](TaskPermissionsDto.md) |  | 
**my_tasks** | [**TaskPermissionsDto**](TaskPermissionsDto.md) |  | 
**created_by_me_tasks** | [**TaskPermissionsDto**](TaskPermissionsDto.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



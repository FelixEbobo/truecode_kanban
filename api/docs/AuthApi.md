# yougileAPI.AuthApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**auth_key_controller_companies_list**](AuthApi.md#auth_key_controller_companies_list) | **POST** /api-v2/auth/companies | Получить список компаний
[**auth_key_controller_create**](AuthApi.md#auth_key_controller_create) | **POST** /api-v2/auth/keys | Создать ключ
[**auth_key_controller_delete**](AuthApi.md#auth_key_controller_delete) | **DELETE** /api-v2/auth/keys/{key} | Удалить ключ
[**auth_key_controller_search**](AuthApi.md#auth_key_controller_search) | **POST** /api-v2/auth/keys/get | Получить список ключей


# **auth_key_controller_companies_list**
> CompanyListDto auth_key_controller_companies_list(credentials_with_name_dto, limit=limit, offset=offset)

Получить список компаний

### Example

```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint

# Enter a context with an instance of the API client
with yougileAPI.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.AuthApi(api_client)
    credentials_with_name_dto = yougileAPI.CredentialsWithNameDto() # CredentialsWithNameDto | 
limit = 50 # float | Количество элементов, которые хочется получить. Максимум 1000. (optional) (default to 50)
offset = 0 # float | Индекс первого элемента страницы (optional) (default to 0)

    try:
        # Получить список компаний
        api_response = api_instance.auth_key_controller_companies_list(credentials_with_name_dto, limit=limit, offset=offset)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AuthApi->auth_key_controller_companies_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **credentials_with_name_dto** | [**CredentialsWithNameDto**](CredentialsWithNameDto.md)|  | 
 **limit** | **float**| Количество элементов, которые хочется получить. Максимум 1000. | [optional] [default to 50]
 **offset** | **float**| Индекс первого элемента страницы | [optional] [default to 0]

### Return type

[**CompanyListDto**](CompanyListDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**401** |  |  -  |
**403** |  |  -  |
**429** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **auth_key_controller_create**
> AuthKeyDto auth_key_controller_create(credentials_with_company_dto)

Создать ключ

### Example

```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint

# Enter a context with an instance of the API client
with yougileAPI.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.AuthApi(api_client)
    credentials_with_company_dto = yougileAPI.CredentialsWithCompanyDto() # CredentialsWithCompanyDto | 

    try:
        # Создать ключ
        api_response = api_instance.auth_key_controller_create(credentials_with_company_dto)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AuthApi->auth_key_controller_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **credentials_with_company_dto** | [**CredentialsWithCompanyDto**](CredentialsWithCompanyDto.md)|  | 

### Return type

[**AuthKeyDto**](AuthKeyDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** |  |  -  |
**400** |  |  -  |
**401** |  |  -  |
**403** |  |  -  |
**429** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **auth_key_controller_delete**
> auth_key_controller_delete(key)

Удалить ключ

### Example

```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint

# Enter a context with an instance of the API client
with yougileAPI.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.AuthApi(api_client)
    key = 'key_example' # str | 

    try:
        # Удалить ключ
        api_instance.auth_key_controller_delete(key)
    except ApiException as e:
        print("Exception when calling AuthApi->auth_key_controller_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **str**|  | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**429** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **auth_key_controller_search**
> list[AuthKeyWithDetailsDto] auth_key_controller_search(credentials_with_company_optional_dto)

Получить список ключей

### Example

```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint

# Enter a context with an instance of the API client
with yougileAPI.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.AuthApi(api_client)
    credentials_with_company_optional_dto = yougileAPI.CredentialsWithCompanyOptionalDto() # CredentialsWithCompanyOptionalDto | 

    try:
        # Получить список ключей
        api_response = api_instance.auth_key_controller_search(credentials_with_company_optional_dto)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AuthApi->auth_key_controller_search: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **credentials_with_company_optional_dto** | [**CredentialsWithCompanyOptionalDto**](CredentialsWithCompanyOptionalDto.md)|  | 

### Return type

[**list[AuthKeyWithDetailsDto]**](AuthKeyWithDetailsDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**400** |  |  -  |
**401** |  |  -  |
**403** |  |  -  |
**429** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


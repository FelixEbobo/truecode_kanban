# CreateProjectDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **str** | Название проекта | 
**users** | [**object**](.md) | Сотрудники на проекте и их роль. Возможные значения: &lt;br/&gt;&lt;div&gt;1) системные роли: worker, admin, observer&lt;/div&gt;&lt;div&gt;2) ID пользовательской роли&lt;/div&gt;&lt;div&gt;3) \&quot;-\&quot; для удаления существующего пользователя из проекта&lt;/div&gt; | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



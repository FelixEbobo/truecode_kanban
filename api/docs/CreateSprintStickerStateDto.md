# CreateSprintStickerStateDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Имя состояния стикера | 
**begin** | **float** | Дата начала спринта в секундах от 01.01.1970 | [optional] 
**end** | **float** | Дата окончания спринта в секундах от 01.01.1970 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



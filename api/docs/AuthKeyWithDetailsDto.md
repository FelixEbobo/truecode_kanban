# AuthKeyWithDetailsDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **str** | Ключ авторизации | 
**company_id** | **str** | ID компании | 
**timestamp** | **float** | Время создания | 
**deleted** | **bool** | Ключ удален - да/нет | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# PagingMetadata

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **float** | Количество элементов в результате | 
**limit** | **float** | Количество элементов на страницу | 
**offset** | **float** | Индекс первого элемента страницы | 
**next** | **bool** | Есть ли элементы после данной страницы | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



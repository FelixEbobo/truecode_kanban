# yougileAPI.WebhooksApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**webhook_controller_create**](WebhooksApi.md#webhook_controller_create) | **POST** /api-v2/webhooks | Создать подписку
[**webhook_controller_put**](WebhooksApi.md#webhook_controller_put) | **PUT** /api-v2/webhooks/{id} | Изменить подписку
[**webhook_controller_search**](WebhooksApi.md#webhook_controller_search) | **GET** /api-v2/webhooks | Получить список подписок


# **webhook_controller_create**
> WithIdDto webhook_controller_create(create_webhook_dto)

Создать подписку

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint
configuration = yougileAPI.Configuration()
# Configure Bearer authorization (JWT): bearer
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to http://localhost
configuration.host = "http://localhost"

# Enter a context with an instance of the API client
with yougileAPI.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.WebhooksApi(api_client)
    create_webhook_dto = yougileAPI.CreateWebhookDto() # CreateWebhookDto | 

    try:
        # Создать подписку
        api_response = api_instance.webhook_controller_create(create_webhook_dto)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling WebhooksApi->webhook_controller_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_webhook_dto** | [**CreateWebhookDto**](CreateWebhookDto.md)|  | 

### Return type

[**WithIdDto**](WithIdDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Подписка создана |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **webhook_controller_put**
> WithIdDto webhook_controller_put(id, update_webhook_dto)

Изменить подписку

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint
configuration = yougileAPI.Configuration()
# Configure Bearer authorization (JWT): bearer
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to http://localhost
configuration.host = "http://localhost"

# Enter a context with an instance of the API client
with yougileAPI.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.WebhooksApi(api_client)
    id = 'id_example' # str | 
update_webhook_dto = yougileAPI.UpdateWebhookDto() # UpdateWebhookDto | 

    try:
        # Изменить подписку
        api_response = api_instance.webhook_controller_put(id, update_webhook_dto)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling WebhooksApi->webhook_controller_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **update_webhook_dto** | [**UpdateWebhookDto**](UpdateWebhookDto.md)|  | 

### Return type

[**WithIdDto**](WithIdDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Подписка обновлена |  -  |
**404** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **webhook_controller_search**
> WebhookDto webhook_controller_search(include_deleted=include_deleted)

Получить список подписок

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint
configuration = yougileAPI.Configuration()
# Configure Bearer authorization (JWT): bearer
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to http://localhost
configuration.host = "http://localhost"

# Enter a context with an instance of the API client
with yougileAPI.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.WebhooksApi(api_client)
    include_deleted = True # bool | По умолчанию, если объект был отмечен как удаленный, то он не будет найден.    Поставьте true, чтобы удаленные объекты возвращались. (optional)

    try:
        # Получить список подписок
        api_response = api_instance.webhook_controller_search(include_deleted=include_deleted)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling WebhooksApi->webhook_controller_search: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **include_deleted** | **bool**| По умолчанию, если объект был отмечен как удаленный, то он не будет найден.    Поставьте true, чтобы удаленные объекты возвращались. | [optional] 

### Return type

[**WebhookDto**](WebhookDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Все подписки |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


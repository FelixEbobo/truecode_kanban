# UpdateDeadline

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deadline** | **float** | Timestamp дэдлайна | [optional] 
**start_date** | **float** | Timestamp начала задачи | [optional] 
**with_time** | **bool** | Отображать на стикере время, или только дату | [optional] 
**deleted** | **bool** | Открепить стикер от задачи (true) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



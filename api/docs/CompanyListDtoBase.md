# CompanyListDtoBase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | ID объекта | 
**name** | **str** | Название компании | 
**is_admin** | **bool** | Права администратора в компании | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



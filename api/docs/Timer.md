# Timer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**seconds** | **float** | Сколько секунд осталось до конца таймера. | 
**since** | **float** | Timestamp момента времени, от которого отсчитывается значение в поле seconds | 
**running** | **bool** | Статус таймера - запущен/остановлен. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



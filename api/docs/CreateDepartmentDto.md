# CreateDepartmentDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **str** | Название отдела | 
**parent_id** | **str** | Id родительского отдела. Оставить пустым или \&quot;-\&quot;, если это отдел верхнего уровня | [optional] 
**users** | [**object**](.md) | Сотрудники на отделе и их роль. Возможные значения: &lt;br/&gt;&lt;div&gt;1) manager или member&lt;/div&gt;&lt;div&gt;2) \&quot;-\&quot; или \&quot;\&quot; для удаления существующего пользователя из отдела&lt;/div&gt; | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



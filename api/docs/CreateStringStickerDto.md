# CreateStringStickerDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Имя стикера | 
**icon** | **str** | Иконка стикера | [optional] 
**states** | [**list[StringStickerStateNoIdDto]**](StringStickerStateNoIdDto.md) | Состояния стикера. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



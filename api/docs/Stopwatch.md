# Stopwatch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**running** | **bool** | Статус секундомера - запущен/остановлен | 
**seconds** | **float** | Сколько секунд прошло, пока таймер был запущен. | 
**at_moment** | **float** | Момент времени, на который значение seconds было актуально | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



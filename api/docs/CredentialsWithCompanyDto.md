# CredentialsWithCompanyDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**login** | **str** | Логин пользователя | 
**password** | **str** | Пароль пользователя | 
**company_id** | **str** | ID компании | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



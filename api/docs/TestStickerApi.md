# yougileAPI.TestStickerApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**string_sticker_controller_create**](TestStickerApi.md#string_sticker_controller_create) | **POST** /api-v2/string-stickers | Создать
[**string_sticker_controller_get**](TestStickerApi.md#string_sticker_controller_get) | **GET** /api-v2/string-stickers/{id} | Получить по ID
[**string_sticker_controller_search**](TestStickerApi.md#string_sticker_controller_search) | **GET** /api-v2/string-stickers | Получить список
[**string_sticker_controller_update**](TestStickerApi.md#string_sticker_controller_update) | **PUT** /api-v2/string-stickers/{id} | Изменить


# **string_sticker_controller_create**
> WithIdDto string_sticker_controller_create(create_string_sticker_dto)

Создать

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint
configuration = yougileAPI.Configuration()
# Configure Bearer authorization (JWT): bearer
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to http://localhost
configuration.host = "http://localhost"

# Enter a context with an instance of the API client
with yougileAPI.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.TestStickerApi(api_client)
    create_string_sticker_dto = yougileAPI.CreateStringStickerDto() # CreateStringStickerDto | 

    try:
        # Создать
        api_response = api_instance.string_sticker_controller_create(create_string_sticker_dto)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling TestStickerApi->string_sticker_controller_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_string_sticker_dto** | [**CreateStringStickerDto**](CreateStringStickerDto.md)|  | 

### Return type

[**WithIdDto**](WithIdDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Стикер создан |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **string_sticker_controller_get**
> StringStickerWithStatesDto string_sticker_controller_get(id)

Получить по ID

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint
configuration = yougileAPI.Configuration()
# Configure Bearer authorization (JWT): bearer
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to http://localhost
configuration.host = "http://localhost"

# Enter a context with an instance of the API client
with yougileAPI.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.TestStickerApi(api_client)
    id = 'id_example' # str | 

    try:
        # Получить по ID
        api_response = api_instance.string_sticker_controller_get(id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling TestStickerApi->string_sticker_controller_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 

### Return type

[**StringStickerWithStatesDto**](StringStickerWithStatesDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**404** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **string_sticker_controller_search**
> StringStickerWithStatesListDto string_sticker_controller_search(include_deleted=include_deleted, limit=limit, offset=offset, name=name, board_id=board_id)

Получить список

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint
configuration = yougileAPI.Configuration()
# Configure Bearer authorization (JWT): bearer
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to http://localhost
configuration.host = "http://localhost"

# Enter a context with an instance of the API client
with yougileAPI.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.TestStickerApi(api_client)
    include_deleted = True # bool | По умолчанию, если объект был отмечен как удаленный, то он не будет найден.    Поставьте true, чтобы удаленные объекты возвращались. (optional)
limit = 50 # float | Количество элементов, которые хочется получить. Максимум 1000. (optional) (default to 50)
offset = 0 # float | Индекс первого элемента страницы (optional) (default to 0)
name = 'name_example' # str | Имя стикера (optional)
board_id = 'board_id_example' # str | ID доски (optional)

    try:
        # Получить список
        api_response = api_instance.string_sticker_controller_search(include_deleted=include_deleted, limit=limit, offset=offset, name=name, board_id=board_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling TestStickerApi->string_sticker_controller_search: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **include_deleted** | **bool**| По умолчанию, если объект был отмечен как удаленный, то он не будет найден.    Поставьте true, чтобы удаленные объекты возвращались. | [optional] 
 **limit** | **float**| Количество элементов, которые хочется получить. Максимум 1000. | [optional] [default to 50]
 **offset** | **float**| Индекс первого элемента страницы | [optional] [default to 0]
 **name** | **str**| Имя стикера | [optional] 
 **board_id** | **str**| ID доски | [optional] 

### Return type

[**StringStickerWithStatesListDto**](StringStickerWithStatesListDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**404** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **string_sticker_controller_update**
> WithIdDto string_sticker_controller_update(id, update_string_sticker_dto)

Изменить

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint
configuration = yougileAPI.Configuration()
# Configure Bearer authorization (JWT): bearer
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to http://localhost
configuration.host = "http://localhost"

# Enter a context with an instance of the API client
with yougileAPI.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.TestStickerApi(api_client)
    id = 'id_example' # str | 
update_string_sticker_dto = yougileAPI.UpdateStringStickerDto() # UpdateStringStickerDto | 

    try:
        # Изменить
        api_response = api_instance.string_sticker_controller_update(id, update_string_sticker_dto)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling TestStickerApi->string_sticker_controller_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **update_string_sticker_dto** | [**UpdateStringStickerDto**](UpdateStringStickerDto.md)|  | 

### Return type

[**WithIdDto**](WithIdDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Стикер изменен |  -  |
**404** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


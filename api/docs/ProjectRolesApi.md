# yougileAPI.ProjectRolesApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**project_roles_controller_create**](ProjectRolesApi.md#project_roles_controller_create) | **POST** /api-v2/projects/{projectId}/roles | Создать
[**project_roles_controller_delete**](ProjectRolesApi.md#project_roles_controller_delete) | **DELETE** /api-v2/projects/{projectId}/roles/{id} | Удалить
[**project_roles_controller_get**](ProjectRolesApi.md#project_roles_controller_get) | **GET** /api-v2/projects/{projectId}/roles/{id} | Получить по ID
[**project_roles_controller_search**](ProjectRolesApi.md#project_roles_controller_search) | **GET** /api-v2/projects/{projectId}/roles | Получить список
[**project_roles_controller_update**](ProjectRolesApi.md#project_roles_controller_update) | **PUT** /api-v2/projects/{projectId}/roles/{id} | Изменить


# **project_roles_controller_create**
> WithIdDto project_roles_controller_create(project_id, create_project_role_dto)

Создать

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint
configuration = yougileAPI.Configuration()
# Configure Bearer authorization (JWT): bearer
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to http://localhost
configuration.host = "http://localhost"

# Enter a context with an instance of the API client
with yougileAPI.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.ProjectRolesApi(api_client)
    project_id = 'project_id_example' # str | 
create_project_role_dto = yougileAPI.CreateProjectRoleDto() # CreateProjectRoleDto | 

    try:
        # Создать
        api_response = api_instance.project_roles_controller_create(project_id, create_project_role_dto)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ProjectRolesApi->project_roles_controller_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project_id** | **str**|  | 
 **create_project_role_dto** | [**CreateProjectRoleDto**](CreateProjectRoleDto.md)|  | 

### Return type

[**WithIdDto**](WithIdDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **project_roles_controller_delete**
> ProjectRoleDto project_roles_controller_delete(project_id, id)

Удалить

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint
configuration = yougileAPI.Configuration()
# Configure Bearer authorization (JWT): bearer
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to http://localhost
configuration.host = "http://localhost"

# Enter a context with an instance of the API client
with yougileAPI.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.ProjectRolesApi(api_client)
    project_id = 'project_id_example' # str | 
id = 'id_example' # str | 

    try:
        # Удалить
        api_response = api_instance.project_roles_controller_delete(project_id, id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ProjectRolesApi->project_roles_controller_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project_id** | **str**|  | 
 **id** | **str**|  | 

### Return type

[**ProjectRoleDto**](ProjectRoleDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**404** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **project_roles_controller_get**
> ProjectRoleDto project_roles_controller_get(project_id, id)

Получить по ID

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint
configuration = yougileAPI.Configuration()
# Configure Bearer authorization (JWT): bearer
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to http://localhost
configuration.host = "http://localhost"

# Enter a context with an instance of the API client
with yougileAPI.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.ProjectRolesApi(api_client)
    project_id = 'project_id_example' # str | 
id = 'id_example' # str | 

    try:
        # Получить по ID
        api_response = api_instance.project_roles_controller_get(project_id, id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ProjectRolesApi->project_roles_controller_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project_id** | **str**|  | 
 **id** | **str**|  | 

### Return type

[**ProjectRoleDto**](ProjectRoleDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**404** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **project_roles_controller_search**
> ProjectRoleListDto project_roles_controller_search(project_id, limit=limit, offset=offset, name=name)

Получить список

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint
configuration = yougileAPI.Configuration()
# Configure Bearer authorization (JWT): bearer
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to http://localhost
configuration.host = "http://localhost"

# Enter a context with an instance of the API client
with yougileAPI.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.ProjectRolesApi(api_client)
    project_id = 'project_id_example' # str | 
limit = 50 # float | Количество элементов, которые хочется получить. Максимум 1000. (optional) (default to 50)
offset = 0 # float | Индекс первого элемента страницы (optional) (default to 0)
name = 'name_example' # str | Имя роли (optional)

    try:
        # Получить список
        api_response = api_instance.project_roles_controller_search(project_id, limit=limit, offset=offset, name=name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ProjectRolesApi->project_roles_controller_search: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project_id** | **str**|  | 
 **limit** | **float**| Количество элементов, которые хочется получить. Максимум 1000. | [optional] [default to 50]
 **offset** | **float**| Индекс первого элемента страницы | [optional] [default to 0]
 **name** | **str**| Имя роли | [optional] 

### Return type

[**ProjectRoleListDto**](ProjectRoleListDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**404** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **project_roles_controller_update**
> WithIdDto project_roles_controller_update(project_id, id, update_project_role_dto)

Изменить

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint
configuration = yougileAPI.Configuration()
# Configure Bearer authorization (JWT): bearer
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to http://localhost
configuration.host = "http://localhost"

# Enter a context with an instance of the API client
with yougileAPI.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.ProjectRolesApi(api_client)
    project_id = 'project_id_example' # str | 
id = 'id_example' # str | 
update_project_role_dto = yougileAPI.UpdateProjectRoleDto() # UpdateProjectRoleDto | 

    try:
        # Изменить
        api_response = api_instance.project_roles_controller_update(project_id, id, update_project_role_dto)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ProjectRolesApi->project_roles_controller_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project_id** | **str**|  | 
 **id** | **str**|  | 
 **update_project_role_dto** | [**UpdateProjectRoleDto**](UpdateProjectRoleDto.md)|  | 

### Return type

[**WithIdDto**](WithIdDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**404** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


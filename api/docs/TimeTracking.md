# TimeTracking

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**plan** | **float** | Сколько часов было запланировано на выполнение задачи | 
**work** | **float** | Сколько часов было затрачено на выполнение задачи | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



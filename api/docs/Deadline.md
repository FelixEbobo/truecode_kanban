# Deadline

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deadline** | **float** | Timestamp дэдлайна | 
**start_date** | **float** | Timestamp начала задачи | [optional] 
**with_time** | **bool** | Отображать на стикере время, или только дату | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



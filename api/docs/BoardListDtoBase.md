# BoardListDtoBase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deleted** | **bool** | Если true, значит объект удален | [optional] 
**id** | **str** | ID объекта | 
**title** | **str** | Название доски | 
**project_id** | **str** | Id проекта, в котором находится доска | 
**stickers** | [**StickersDto**](StickersDto.md) | Стикеры доски | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# UpdateStopwatch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**running** | **bool** | Запустить или остановить секундомер | [optional] 
**deleted** | **bool** | Открепить стикер от задачи (true) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



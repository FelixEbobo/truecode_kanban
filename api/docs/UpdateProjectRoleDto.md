# UpdateProjectRoleDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Название роли | [optional] 
**description** | **str** | Описание роли | [optional] 
**permissions** | [**ProjectPermissionsDto**](ProjectPermissionsDto.md) | Права в проекте | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



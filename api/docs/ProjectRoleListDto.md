# ProjectRoleListDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**paging** | [**PagingMetadata**](PagingMetadata.md) | Дополнительная информация о странице | 
**content** | [**list[ProjectRoleListDtoBase]**](ProjectRoleListDtoBase.md) | Список ролей в проекте | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# GroupChatListDtoBase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deleted** | **bool** | Если true, значит объект удален | [optional] 
**id** | **str** | ID объекта | 
**title** | **str** | Название чата | 
**users** | [**object**](.md) | Сотрудники в чате | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



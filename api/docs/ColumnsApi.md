# yougileAPI.ColumnsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**column_controller_create**](ColumnsApi.md#column_controller_create) | **POST** /api-v2/columns | Создать
[**column_controller_get**](ColumnsApi.md#column_controller_get) | **GET** /api-v2/columns/{id} | Получить по ID
[**column_controller_search**](ColumnsApi.md#column_controller_search) | **GET** /api-v2/columns | Получить список
[**column_controller_update**](ColumnsApi.md#column_controller_update) | **PUT** /api-v2/columns/{id} | Изменить


# **column_controller_create**
> WithIdDto column_controller_create(create_column_dto)

Создать

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint
configuration = yougileAPI.Configuration()
# Configure Bearer authorization (JWT): bearer
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to http://localhost
configuration.host = "http://localhost"

# Enter a context with an instance of the API client
with yougileAPI.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.ColumnsApi(api_client)
    create_column_dto = yougileAPI.CreateColumnDto() # CreateColumnDto | 

    try:
        # Создать
        api_response = api_instance.column_controller_create(create_column_dto)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ColumnsApi->column_controller_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_column_dto** | [**CreateColumnDto**](CreateColumnDto.md)|  | 

### Return type

[**WithIdDto**](WithIdDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **column_controller_get**
> ColumnDto column_controller_get(id)

Получить по ID

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint
configuration = yougileAPI.Configuration()
# Configure Bearer authorization (JWT): bearer
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to http://localhost
configuration.host = "http://localhost"

# Enter a context with an instance of the API client
with yougileAPI.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.ColumnsApi(api_client)
    id = 'id_example' # str | 

    try:
        # Получить по ID
        api_response = api_instance.column_controller_get(id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ColumnsApi->column_controller_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 

### Return type

[**ColumnDto**](ColumnDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**404** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **column_controller_search**
> ColumnListDto column_controller_search(include_deleted=include_deleted, limit=limit, offset=offset, title=title, board_id=board_id)

Получить список

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint
configuration = yougileAPI.Configuration()
# Configure Bearer authorization (JWT): bearer
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to http://localhost
configuration.host = "http://localhost"

# Enter a context with an instance of the API client
with yougileAPI.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.ColumnsApi(api_client)
    include_deleted = True # bool | По умолчанию, если объект был отмечен как удаленный, то он не будет найден.    Поставьте true, чтобы удаленные объекты возвращались. (optional)
limit = 50 # float | Количество элементов, которые хочется получить. Максимум 1000. (optional) (default to 50)
offset = 0 # float | Индекс первого элемента страницы (optional) (default to 0)
title = 'title_example' # str | Имя колонки (optional)
board_id = 'board_id_example' # str | ID доски (optional)

    try:
        # Получить список
        api_response = api_instance.column_controller_search(include_deleted=include_deleted, limit=limit, offset=offset, title=title, board_id=board_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ColumnsApi->column_controller_search: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **include_deleted** | **bool**| По умолчанию, если объект был отмечен как удаленный, то он не будет найден.    Поставьте true, чтобы удаленные объекты возвращались. | [optional] 
 **limit** | **float**| Количество элементов, которые хочется получить. Максимум 1000. | [optional] [default to 50]
 **offset** | **float**| Индекс первого элемента страницы | [optional] [default to 0]
 **title** | **str**| Имя колонки | [optional] 
 **board_id** | **str**| ID доски | [optional] 

### Return type

[**ColumnListDto**](ColumnListDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**404** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **column_controller_update**
> WithIdDto column_controller_update(id, update_column_dto)

Изменить

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint
configuration = yougileAPI.Configuration()
# Configure Bearer authorization (JWT): bearer
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to http://localhost
configuration.host = "http://localhost"

# Enter a context with an instance of the API client
with yougileAPI.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.ColumnsApi(api_client)
    id = 'id_example' # str | 
update_column_dto = yougileAPI.UpdateColumnDto() # UpdateColumnDto | 

    try:
        # Изменить
        api_response = api_instance.column_controller_update(id, update_column_dto)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ColumnsApi->column_controller_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **update_column_dto** | [**UpdateColumnDto**](UpdateColumnDto.md)|  | 

### Return type

[**WithIdDto**](WithIdDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**404** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


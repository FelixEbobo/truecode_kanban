# yougileAPI.TextStickerStateApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**string_sticker_state_controller_create**](TextStickerStateApi.md#string_sticker_state_controller_create) | **POST** /api-v2/string-stickers/{stickerId}/states | Создать
[**string_sticker_state_controller_get**](TextStickerStateApi.md#string_sticker_state_controller_get) | **GET** /api-v2/string-stickers/{stickerId}/states/{stickerStateId} | Получить по ID
[**string_sticker_state_controller_update**](TextStickerStateApi.md#string_sticker_state_controller_update) | **PUT** /api-v2/string-stickers/{stickerId}/states/{stickerStateId} | Изменить


# **string_sticker_state_controller_create**
> WithStickerStateIdDto string_sticker_state_controller_create(sticker_id, create_string_sticker_state_dto)

Создать

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint
configuration = yougileAPI.Configuration()
# Configure Bearer authorization (JWT): bearer
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to http://localhost
configuration.host = "http://localhost"

# Enter a context with an instance of the API client
with yougileAPI.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.TextStickerStateApi(api_client)
    sticker_id = 'sticker_id_example' # str | 
create_string_sticker_state_dto = yougileAPI.CreateStringStickerStateDto() # CreateStringStickerStateDto | 

    try:
        # Создать
        api_response = api_instance.string_sticker_state_controller_create(sticker_id, create_string_sticker_state_dto)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling TextStickerStateApi->string_sticker_state_controller_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sticker_id** | **str**|  | 
 **create_string_sticker_state_dto** | [**CreateStringStickerStateDto**](CreateStringStickerStateDto.md)|  | 

### Return type

[**WithStickerStateIdDto**](WithStickerStateIdDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Состояние стикера успешно создано. |  -  |
**404** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **string_sticker_state_controller_get**
> StringStickerStateDto string_sticker_state_controller_get(sticker_id, sticker_state_id, include_deleted=include_deleted)

Получить по ID

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint
configuration = yougileAPI.Configuration()
# Configure Bearer authorization (JWT): bearer
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to http://localhost
configuration.host = "http://localhost"

# Enter a context with an instance of the API client
with yougileAPI.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.TextStickerStateApi(api_client)
    sticker_id = 'sticker_id_example' # str | 
sticker_state_id = 'sticker_state_id_example' # str | 
include_deleted = True # bool |  (optional)

    try:
        # Получить по ID
        api_response = api_instance.string_sticker_state_controller_get(sticker_id, sticker_state_id, include_deleted=include_deleted)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling TextStickerStateApi->string_sticker_state_controller_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sticker_id** | **str**|  | 
 **sticker_state_id** | **str**|  | 
 **include_deleted** | **bool**|  | [optional] 

### Return type

[**StringStickerStateDto**](StringStickerStateDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**404** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **string_sticker_state_controller_update**
> WithStickerStateIdDto string_sticker_state_controller_update(sticker_id, sticker_state_id, update_string_sticker_state_dto)

Изменить

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import yougileAPI
from yougileAPI.rest import ApiException
from pprint import pprint
configuration = yougileAPI.Configuration()
# Configure Bearer authorization (JWT): bearer
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to http://localhost
configuration.host = "http://localhost"

# Enter a context with an instance of the API client
with yougileAPI.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = yougileAPI.TextStickerStateApi(api_client)
    sticker_id = 'sticker_id_example' # str | 
sticker_state_id = 'sticker_state_id_example' # str | 
update_string_sticker_state_dto = yougileAPI.UpdateStringStickerStateDto() # UpdateStringStickerStateDto | 

    try:
        # Изменить
        api_response = api_instance.string_sticker_state_controller_update(sticker_id, sticker_state_id, update_string_sticker_state_dto)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling TextStickerStateApi->string_sticker_state_controller_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sticker_id** | **str**|  | 
 **sticker_state_id** | **str**|  | 
 **update_string_sticker_state_dto** | [**UpdateStringStickerStateDto**](UpdateStringStickerStateDto.md)|  | 

### Return type

[**WithStickerStateIdDto**](WithStickerStateIdDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Состояние стикера успешно изменено. |  -  |
**404** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


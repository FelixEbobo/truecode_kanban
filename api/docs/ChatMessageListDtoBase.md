# ChatMessageListDtoBase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deleted** | **bool** | Если true, значит объект удален | [optional] 
**id** | **float** | ID сообщения, также является временем создания | 
**from_user_id** | **str** | ID автора сообщения | 
**text** | **str** | Текст сообщения | 
**label** | **str** | Быстрая ссылка | 
**edit_timestamp** | **float** | Время последнего редактирования | 
**reactions** | [**object**](.md) | Реакции на сообщение | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



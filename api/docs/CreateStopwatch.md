# CreateStopwatch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**running** | **bool** | Запустить или остановить секундомер | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# UpdateGroupChatDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deleted** | **bool** | Если true, значит объект удален | [optional] 
**title** | **str** | Название чата | [optional] 
**users** | [**object**](.md) | Сотрудники в чате | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



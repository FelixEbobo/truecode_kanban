import datetime

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, TIMESTAMP

Base = declarative_base()

class Task(Base):
    __tablename__ = "task"

    def convert_to_yougile_params(self):
        pass

    def convert_to_bitrix_params(self):
        pass

    def set_bitrix_id(self, bitrix_id: int):
        pass

    def set_yougile_id(self, yougile_id: int):
        pass

class Project(Base):
    __tablename__ = "project"

class Comment(Base):
    __tablename__ = "comment"
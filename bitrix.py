from fast_bitrix24 import Bitrix as BitrixAPI
from settings import JsonSettings
from urllib.parse import unquote, parse_qs


class Bitrix:
    def __init__(self) -> None:
        settings = JsonSettings()
        self.api_client = BitrixAPI(settings.get_setting("bitrix.webhook"))
        self.api_client.get_by_ID()

    def get_task(self, task_id: int):
        """Получаем задачу из сервера битрикса"""
        return {}

    def create_task(self):
        pass

    def update_task(self, task_id: int):
        pass

    def delete_task(self, task_id: int):
        pass

    def create_project(self):
        pass

    def update_project(self, project_id: int):
        pass

    def add_comment(self):
        pass

    def update_comment(self, comment_it: int):
        pass

    def delete_comment(self, comment_id: int):
        pass

data = parse_qs('event=ONTASKUPDATE&data%5BFIELDS_BEFORE%5D%5BID%5D=40003&data%5BFIELDS_AFTER%5D%5BID%5D=40003&data%5BIS_ACCESSIBLE_BEFORE%5D=undefined&data%5BIS_ACCESSIBLE_AFTER%5D=undefined&ts=1671351541&auth%5Bdomain%5D=truecode.bitrix24.ru&auth%5Bclient_endpoint%5D=https%3A%2F%2Ftruecode.bitrix24.ru%2Frest%2F&auth%5Bserver_endpoint%5D=https%3A%2F%2Foauth.bitrix.info%2Frest%2F&auth%5Bmember_id%5D=7ead9a280610d866dbc7c535d1521295&auth%5Bapplication_token%5D=yp71ffgv4w0zoo7ygfisplc7ar5i1jl1')
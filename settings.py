import json
import logging
from typing import Dict, Any

class JsonSettings:
    """Class for settings manipulation"""

    def __init__(self):
        self.data = {}
        logging.debug("open setting.json file")
        with open('settings.json', 'r', encoding='utf-8') as f:
            logging.info("settings loaded")
            self.data = json.load(f)

    # TODO(felix_ebobo): Сделать get_setting для nested тоже
    def get_setting(self, key: str, default = None) -> Dict[str, Any] | str | int | None:
        """read setting from settings.json under given key"""
        logging.debug(f"trying to get {key}")

        return self.data.get(key, default)

    def set_setting(self, key: str, value: str | int | None) -> None:
        """
        Set value for setting under the given key from settings.json
        If you want to set nested value, specify keys joined with dot
        example: a.b will set key for {a: {b: value}} 
        """
        logging.debug(f"setting {key}: {value}")

        keys = key.split('.')
        data = None
        for index, key in enumerate(keys):
            if index + 1 == len(keys):
                data[key] = value
            else:
                data = self.data[key]

        self.__save_settings_file()

    def __save_settings_file(self) -> None:
        logging.info("saving settings file")

        with open("settings.json", 'w', encoding="utf-8") as f:
            json.dump(self.data, f, indent=4, ensure_ascii=False)

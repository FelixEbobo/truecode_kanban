from settings import JsonSettings
import logging

from yougileAPI.api_client import ApiClient, Configuration
from yougileAPI.api import AuthApi, BoardsApi
from yougileAPI.models import CredentialsWithNameDto, CompanyListDto, CompanyListDtoBase, AuthKeyWithDetailsDto, CredentialsWithCompanyOptionalDto, CredentialsWithCompanyDto, AuthKeyDto

def make_request(suffix: str):
    return YouGile.API_URL + suffix

class YouGile:
    API_URL = 'https://yougile.com'

    def __init__(self) -> None:
        settings = JsonSettings()
        self.api_client = ApiClient(Configuration(host=YouGile.API_URL))
        self.company_id: str = None

        yougile = settings.get_setting('yougile')
        if not yougile.get("token"):
            self.authorize_and_set_token(yougile["login"], yougile["password"])
        else:
            self.set_token(yougile["token"])

        self.company_id = yougile["company_id"]

    def authorize_and_set_token(self, login: str, password: str) -> str:
        logging.debug(self.authorize_and_set_token.__name__)

        auth_params = CredentialsWithNameDto(login=login, password=password)
        response: CompanyListDto = AuthApi(self.api_client).auth_key_controller_companies_list(auth_params)
        company_base: CompanyListDtoBase = response.content[0]

        self.company_id = company_base.id
        JsonSettings().set_setting('yougile.company_id', company_base.id)

        auth_params = CredentialsWithCompanyDto(login=login, password=password, company_id=company_base.id)
        response: AuthKeyDto = AuthApi(self.api_client).auth_key_controller_create(auth_params)
        self.api_client.configuration.access_token = response.key
        JsonSettings().set_setting('yougile.token', response.key)


    def set_token(self, token: str):
        logging.debug(self.set_token.__name__)

        self.api_client.configuration.access_token = token

    def get_boards_list(self):
        logging.info(BoardsApi(self.api_client).board_controller_search())

    def get_task(self, task_id: int):
        return {}

    def create_task(self):
        pass

    def update_task(self, task_id: int):
        pass

    def delete_task(self, task_id: int):
        pass

    def create_project(self):
        pass

    def update_project(self, project_id: int):
        pass

    def add_comment(self):
        pass

    def update_comment(self, comment_it: int):
        pass

    def delete_comment(self, comment_id: int):
        pass
